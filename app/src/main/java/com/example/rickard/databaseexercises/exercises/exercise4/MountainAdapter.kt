package com.example.rickard.databaseexercises.exercises.exercise4

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.rickard.databaseexercises.data.exercise4.Mountain

class MountainAdapter: RecyclerView.Adapter<MountainViewHolder>() {

    private var mountains:List<Mountain> = ArrayList()
    private var onMountainClicked: MountainViewHolder.OnMountainClickedListener? = null

    fun setMountains(mountains:List<Mountain>) {
        this.mountains = mountains
        notifyDataSetChanged()
    }

    fun setOnMountainClickListener(onClickListener: MountainViewHolder.OnMountainClickedListener) {
        this.onMountainClicked = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): MountainViewHolder = MountainViewHolder.newInstance(parent, onMountainClicked)

    override fun getItemCount(): Int = mountains.size

    override fun onBindViewHolder(viewHolder: MountainViewHolder, position: Int) = viewHolder.bind(mountains[position])
}