package com.example.rickard.databaseexercises.data.exercise2

data class DataGSON (
    var id : Int,
    var description : String,
    var ratio: Float
)
