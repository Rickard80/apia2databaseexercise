package com.example.rickard.databaseexercises.exercises.exercise4

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.rickard.databaseexercises.R
import com.example.rickard.databaseexercises.data.exercise4.Mountain

class MountainViewHolder(view: View, listener: OnMountainClickedListener?) : RecyclerView.ViewHolder(view) {

    var mountain: Mountain? = null

    val mountain_name: TextView = view.findViewById(R.id.mountain_name)
    val mountain_height: TextView = view.findViewById((R.id.mountain_height))

    init {
        view.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                mountain?.let { user ->
                    listener?.onMountainClicked(user)
                }
            }
        })
    }

    fun bind(mountain: Mountain) {
        this.mountain = mountain

        mountain_name.text = mountain.name
        mountain_height.text = mountain.height.toString() + " m"
    }

    interface OnMountainClickedListener {
        fun onMountainClicked(mountain: Mountain)
    }

    companion object {
        fun newInstance(parent: ViewGroup, listener: OnMountainClickedListener?) : MountainViewHolder {
            return MountainViewHolder(
                    LayoutInflater.from(parent.context).inflate(R.layout.exercise4_mountain_list_item, parent, false),
                    listener
            )
        }
    }
}