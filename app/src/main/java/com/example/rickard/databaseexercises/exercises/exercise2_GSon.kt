package com.example.rickard.databaseexercises.exercises

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.rickard.databaseexercises.R
import com.example.rickard.databaseexercises.data.exercise2.DataGSON
import com.google.gson.Gson

class exercise2_GSon : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise2__gson)

        val loggingString = "testing"
        val prefKey = "dataGSON"

        var jsonStr = readObject(prefKey)
        val obj = if (jsonStr != "") convertFromJSON(jsonStr) else DataGSON(0, "BESKRIVNING", 2.4f)
        obj.id++

        jsonStr = convertToJSon(obj)
        Log.d(loggingString, "JSON String " + jsonStr)

        saveObject(prefKey, jsonStr)

//        clearObject(prefKey)
//        Log.d("testing", "Cleared Storage " + readObject())

    }

        // GSon

    fun convertToJSon(obj: DataGSON) : String {
        val gson = Gson()
        return gson.toJson(obj)
    }

    fun convertFromJSON(jsonStr: String) : DataGSON {
        val gson = Gson()

        return gson.fromJson(jsonStr, DataGSON::class.java)
    }

        // Shared Preferences

    fun getSharedPref() : SharedPreferences {
        return getSharedPreferences("myPref", Context.MODE_PRIVATE)
    }

    fun readObject(key: String) : String {
        val sharedPref = getSharedPref()
        val jsonObj = sharedPref.getString(key, "")

        return jsonObj
    }

    fun saveObject(key: String, objJSON: String) {
        val sharedPref = getSharedPref()
        val editor = sharedPref.edit()
        editor.putString(key, objJSON)
        editor.apply()
    }

    fun clearObject(key: String) {
        val sharedPref = getSharedPref()
        val editor = sharedPref.edit()

        editor.remove(key)
        editor.apply()
    }
}
