package com.example.rickard.databaseexercises.exercises

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.rickard.databaseexercises.R

class exercise1_sharedPreferences : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.shared_preferences_activity)

        val storageKey = "myPref";
        val valueKey = "increasingNumber";

        val sharedPref = getSharedPreferences(storageKey, Context.MODE_PRIVATE)
        var value = sharedPref.getInt(valueKey, 0);

        Log.d("counter", value.toString());

        value++;

        val prefEditor = sharedPref.edit()
        prefEditor.putInt(valueKey, value)
        prefEditor.apply()
    }
}
