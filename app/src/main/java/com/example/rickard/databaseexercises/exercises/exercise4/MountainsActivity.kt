package com.example.rickard.databaseexercises.exercises.exercise4

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.widget.TextView
import com.example.rickard.databaseexercises.R
import com.example.rickard.databaseexercises.data.exercise4.Mountain
import com.example.rickard.databaseexercises.data.exercise4.MountainDatabase
import com.example.rickard.databaseexercises.utils.IO
import com.example.rickard.databaseexercises.utils.UI
import kotlinx.android.synthetic.main.exercise4_database_activity.*

class MountainsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exercise4_database_activity)

        val adapter = MountainAdapter()

        adapter.setOnMountainClickListener(object : MountainViewHolder.OnMountainClickedListener {
            override fun onMountainClicked(mountain: Mountain) {
                delete(mountain, object: Callback {
                    override fun onSuccess() {
                        updateList(adapter)
                    }
                })
            }
        })

        mountain_recycler_view.layoutManager = GridLayoutManager(this, 2)
        mountain_recycler_view.adapter = adapter

        updateList(adapter)

        add_mountain.setOnClickListener {
            showCreateMountainDialog(adapter)
        }
    }

    private fun showCreateMountainDialog(adapter: MountainAdapter) {
        val builder = AlertDialog.Builder(this)
        val view = layoutInflater.inflate(R.layout.exercise5_create_mountain_dialog, null)
        val mountainNameTextView = view.findViewById(R.id.dialog_mountain_name) as TextView
        val mountainHeightTextView = view.findViewById(R.id.dialog_mountain_height) as TextView

        builder.setView(view)
        builder.setTitle(R.string.addMountain)
        builder.setPositiveButton(android.R.string.ok) {
            dialog, p ->
            if (! (mountainNameTextView.text.isNullOrBlank() || mountainHeightTextView.text.isNullOrBlank() )) {
                var newMountain = Mountain(mountainNameTextView.text.toString(), mountainHeightTextView.text.toString().toLong())
                saveMountainInformation(newMountain, object : Callback {
                    override fun onSuccess() {
                        updateList(adapter)
                    }

                })
            }
        }

        builder.show()
    }


    /* DATABASE HANDLING */

    fun updateList(adapter: MountainAdapter) {
        getAllMountains(object : OnMountainRead {
            override fun onSuccess(mountains: List<Mountain>) {
                adapter.setMountains(mountains)
            }
        })
    }

    fun saveMountainInformation(mountain: Mountain, listener: Callback) {
        IO.execute {
            MountainDatabase.getInstance(this)?.mountainDao?.create(mountain)
            UI.execute {
                listener.onSuccess()
            }
        }
    }

    fun getAllMountains(listener: OnMountainRead) {
        IO.execute {
            val mountains = MountainDatabase.getInstance(this)?.mountainDao?.getAll() ?: ArrayList<Mountain>();
            UI.execute {
                listener.onSuccess(mountains)
            }
        }
    }

    fun delete(mountain: Mountain, listener: Callback) {
        IO.execute {
            MountainDatabase.getInstance(this)?.mountainDao?.delete(mountain)
            UI.execute {
                listener.onSuccess()
            }
        }
    }

//    fun updateMountainInformation(mountain: Mountain) {
//        IO.execute {
//            MountainDatabase.getInstance(this)?.mountainDao?.update(mountain)
//        }
//    }

    interface OnMountainRead {
        fun onSuccess(mountains: List<Mountain>)
    }

    interface Callback {
        fun onSuccess()
    }
}
