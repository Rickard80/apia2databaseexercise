package com.example.rickard.databaseexercises.data.exercise4

import android.arch.persistence.room.*
import com.example.rickard.databaseexercises.data.exercise4.Mountain

@Dao
interface MountainDao {
    @Insert
    fun create(vararg mountains: Mountain)

    @Query("Select * FROM mountains")
    fun getAll(): List<Mountain>

    @Update
    fun update(vararg mountains: Mountain)

    @Delete
    fun delete(vararg mountains: Mountain)
}