package com.example.rickard.databaseexercises.data.exercise4

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(Mountain::class), version = 1, exportSchema = false)
abstract class MountainDatabase : RoomDatabase() {

    abstract val mountainDao: MountainDao

    companion object {
        private var INSTANCE: MountainDatabase? = null

        fun getInstance(context: Context): MountainDatabase? {
            if (INSTANCE == null) {
                synchronized(MountainDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MountainDatabase::class.java, "database.db")
//                            .allowMainThreadQueries()
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

//    fun create(mountain: Mountain) {
//        mountainDao.create(mountain)
//    }
//
//    fun getAll() : List<Mountain> {
//        return mountainDao.getAll()
//    }
//
//    fun update(mountain: Mountain) {
//        mountainDao.update()
//    }
//
//    fun delete(mountain: Mountain) {
//        mountainDao.delete(mountain)
//    }

}