package com.example.rickard.databaseexercises.data.exercise4

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "mountains")
data class Mountain(
    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "height")
    var height: Long
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
}