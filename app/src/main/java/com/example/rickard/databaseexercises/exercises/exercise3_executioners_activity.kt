package com.example.rickard.databaseexercises.exercises

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.rickard.databaseexercises.R
import com.example.rickard.databaseexercises.utils.UI
import kotlinx.android.synthetic.main.exercise3_executioners_activity.*
import java.util.*
import kotlin.concurrent.schedule

class exercise3_executioners_activity : AppCompatActivity() {

    var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exercise3_executioners_activity)

//        IO.execute {
//            Thread.sleep(2000)
//            UI.execute {
//                textview_print.text = "Woohoo"
//            }
//        }
        startCounter(0)
    }

    private fun startCounter(startTimeInSeconds: Long) {
        val timer = Timer("schedule", true)

        updateTextFieldWithCounter()

        timer.schedule((startTimeInSeconds+1)*1000, 1000) {
            UI.execute {
                counter++;
                updateTextFieldWithCounter();
            }
        }
    }

    private fun updateTextFieldWithCounter() {
        textview_print.text = counter.toString();
    }
}
